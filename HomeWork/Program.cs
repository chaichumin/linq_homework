﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork
{

    class Program
    {

        static void Main(string[] args)
        {

            var p = new Program();

            p.SelectData();

            p.InsertData();

            p.UpdateData();

            p.DeleteData();
            

        }
        public void SelectData()
        {
            Console.WriteLine(">>>>  start select COUNTRY_CODE == US  <<<<");
            using (var db = new DB_LearningEntities())
            {
                string InName = "j";
                var en = db.CUSTOMER.Where(z => z.COUNTRY_CODE == "US").ToList();

                foreach (var item in en)
                {
                    Console.WriteLine(string.Format("{0}|{1}|{2}|{3}|{4}|{5}"
                     , item.CUSTOMER_ID
                     , item.NAME
                     , item.EMAIL
                     , item.COUNTRY_CODE
                     , item.BUDGET
                     , item.USED));

                }



            }
            Console.WriteLine("pless any key to insert data");
            Console.ReadLine();
        }
        public void InsertData()
        {
            Console.WriteLine(">>>>  start insert data to CUSTOMER TABLE  <<<<");
            using (var db = new DB_LearningEntities())
            {
                try
                {
                    var insert = new CUSTOMER();
                    insert.CUSTOMER_ID = "C005";
                    insert.NAME = "Weera mark";
                    insert.EMAIL = "weeramark@gmail.com";
                    insert.COUNTRY_CODE = "TH";
                    insert.BUDGET = 5000000;
                    insert.USED = 0;
                    db.CUSTOMER.Add(insert);

                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.InnerException.InnerException.Message);
                }


                Console.WriteLine("====== show data inserted ======");
                var en = db.CUSTOMER.Where(z => z.NAME == "Weera mark").ToList();
                foreach (var item in en)
                {
                    Console.WriteLine(string.Format("{0}|{1}|{2}|{3}|{4}|{5}"
                     , item.CUSTOMER_ID
                     , item.NAME
                     , item.EMAIL
                     , item.COUNTRY_CODE
                     , item.BUDGET
                     , item.USED));

                }

            }
            Console.WriteLine("pless any key to update data");
            Console.ReadLine();
        }
        public void UpdateData()
        {
            Console.WriteLine(">>>>  start update data in CUSTOMER TABLE  <<<<");
            using (var db = new DB_LearningEntities())
            {
                try
                {

                    string Name = "j";
                    var update = (from c in db.CUSTOMER
                                  where c.NAME.Contains(Name)
                                  select c).ToList();
                    if (update != null)
                    {
                        foreach (var item in update)
                        {
                            item.BUDGET = 0;
                            item.USED = 1;
                        }
                    }

                    db.SaveChanges();

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.InnerException.InnerException.Message);
                }


                Console.WriteLine("====== show data Updated ======");
                string InName = "j";
                var en = db.CUSTOMER.Where(z => z.NAME.Contains(InName)).ToList();
                foreach (var item in en)
                {
                    Console.WriteLine(string.Format("{0}|{1}|{2}|{3}|{4}|{5}"
                     , item.CUSTOMER_ID
                     , item.NAME
                     , item.EMAIL
                     , item.COUNTRY_CODE
                     , item.BUDGET
                     , item.USED));

                }
            }
            Console.WriteLine("pless any key to delete data");
            Console.ReadLine();
        }
        public void DeleteData()
        {
            Console.WriteLine(">>>>  start Delete data in last insert  <<<<");
            using (var db = new DB_LearningEntities())
            {
                try
                {
                    string strCustomerID = "C005";
                    var del = (from c in db.CUSTOMER
                               where c.CUSTOMER_ID == strCustomerID
                               select c).FirstOrDefault();
                    if (del != null)
                    {
                        db.CUSTOMER.Remove(del);
                    }

                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.InnerException.InnerException.Message);
                }


                Console.WriteLine("====== show data ======");
                var en = db.CUSTOMER.Where(z => z.CUSTOMER_ID == "c005").ToList();
                if(en.Count == 0)
                {
                    Console.WriteLine("Data is null");
                }
                else
                {
                    foreach (var item in en)
                    {

                        Console.WriteLine(string.Format("{0}|{1}|{2}|{3}|{4}|{5}"
                         , item.CUSTOMER_ID
                         , item.NAME
                         , item.EMAIL
                         , item.COUNTRY_CODE
                         , item.BUDGET
                         , item.USED));

                    }
                }

            }
            Console.WriteLine("pless any key to close");
            Console.ReadLine();
        }




    }
}
